package com.mokhtarmial.scouternotificationsgateway;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class MainActivity extends AppCompatActivity {
    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";

    private TextView mTextNotifications;
    private NotificationReceiver notificationReceiver;
    private ArrayList<String> received = new ArrayList<>();
    private WebSocket ws;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextNotifications = findViewById(R.id.notifications);
        if (!isNotificationServiceEnabled()) {
            buildNotificationServiceAlertDialog().show();
        }
        notificationReceiver = new NotificationReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.mokhtarmial.scouternotificationsgateway");
        registerReceiver(notificationReceiver, intentFilter);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("ws://165.227.237.182:3029").build();
        ws = client.newWebSocket(request, new WebSocketListener() {
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(notificationReceiver);
        ws.close(29, "done");
    }

    class NotificationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String origin = intent.getStringExtra("origin");
            String title = intent.getStringExtra("title");
            String content = intent.getStringExtra("content");

            JSONObject json = new JSONObject();
            try {
                json.put("origin", origin);
                json.put("title", title);
                json.put("content", content);
            } catch (Exception e) {
                Log.d("JSON", "lol");
            }
            ByteString bs = ByteString.of(json.toString().getBytes());
            ws.send(bs);

            received.add(origin + ": " + title + " - " + content);
            mTextNotifications.setText(String.join("\n\n", received));
        }
    }

    private boolean isNotificationServiceEnabled() {
        final String pkgName = getPackageName();
        final String flat = Settings.Secure.getString(getContentResolver(), ENABLED_NOTIFICATION_LISTENERS);
        return flat != null && flat.contains(pkgName);
    }

    private AlertDialog buildNotificationServiceAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.notification_listener_service);
        alertDialogBuilder.setMessage(R.string.notification_listener_service_explanation);
        alertDialogBuilder.setPositiveButton(R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS));
                    }
                }
        );
        alertDialogBuilder.setNegativeButton(R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // If you choose to not enable the notification listener
                        // the app. will not work as expected
                    }
                }
        );
        return (alertDialogBuilder.create());
    }
}
