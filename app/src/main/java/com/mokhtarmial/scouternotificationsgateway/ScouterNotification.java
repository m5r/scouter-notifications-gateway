package com.mokhtarmial.scouternotificationsgateway;

class ScouterNotification {
    private String origin;
    private String title;
    private String content;

    ScouterNotification(String origin, String title, String content) {
        this.origin = origin;
        this.title = title;
        this.content = content;
    }

    String getOrigin() {
        return origin;
    }

    String getTitle() {
        return title;
    }

    String getContent() {
        return content;
    }
}
