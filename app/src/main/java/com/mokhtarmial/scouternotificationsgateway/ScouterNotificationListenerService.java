package com.mokhtarmial.scouternotificationsgateway;

import android.content.Intent;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import static android.app.Notification.EXTRA_SUB_TEXT;
import static android.app.Notification.EXTRA_TEXT;
import static android.app.Notification.EXTRA_TITLE;

public class ScouterNotificationListenerService extends NotificationListenerService {
    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        Intent intent = new Intent("com.mokhtarmial.scouternotificationsgateway");

        String origin = statusBarNotification.getPackageName();
        String title = statusBarNotification.getNotification().extras.getString(EXTRA_TITLE);
        String content = statusBarNotification.getNotification().extras.getString(EXTRA_TEXT);

        intent.putExtra("origin", origin);
        intent.putExtra("title", title);
        intent.putExtra("content", content);

        sendBroadcast(intent);
    }
}
